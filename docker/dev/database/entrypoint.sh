#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "residency" <<-EOSQL
    CREATE DATABASE residency_test;
    GRANT ALL PRIVILEGES ON DATABASE residency_test TO residency;
EOSQL