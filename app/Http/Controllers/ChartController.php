<?php
declare(strict_types=1);

namespace App\Http\Controllers;


use App\SensorData;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChartController
{

    public function mainPage()
    {
        return view('main');
    }

    public function storeData(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            SensorData::create([
                'name' => $key,
                'value' => $value,
                'created_at' => Carbon::now()
            ]);
        }
        return json_encode($request->all());
    }

    public function getData(Request $request)
    {
        $queryBuilder = SensorData::select('name', 'value')->groupBy('name', 'value', 'id')->orderBy('id', 'desc');

        if ($request->input('period') && $request->input('period') > 0)
        {
            $queryBuilder->where('created_at', '>', Carbon::now()->addMinutes($request->input('period') * -1));
        }

        return $queryBuilder->get()->groupBy('name')->map(function ($sensor) {
            return array_column($sensor->toArray(), 'value');
        });
    }
}