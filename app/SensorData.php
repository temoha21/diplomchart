<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{
    protected $fillable = [
        'value', 'name', 'created_at'
    ];
    public $timestamps = false;

    protected $hidden = [
        'created_at'
    ];
}
