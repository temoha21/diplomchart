<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\SensorData;
use Faker\Generator as Faker;

$factory->define(SensorData::class, function (Faker $faker) {
    $name = $faker->randomElement(['temperature', 'humidity', 'light']);
    $value = 0;
    if ($name === 'temperature')
        $value = $faker->randomFloat(1, 15, 45);
    if ($name === 'humidity')
        $value = $faker->randomFloat(1, 0, 100);
    if ($name === 'light')
        $value = $faker->randomFloat(1, 150, 1000);
    return [
        'name' => $name,
        'value' => $value
    ];
});